import { Component, OnInit, Input } from '@angular/core';
import { Client } from '../client';
import { Investor } from '../investor';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { ClientService } from '../client.service';
import {InvestorService} from '../investor.service';

@Component({
  selector: 'app-client-detail',
  templateUrl: './client-detail.component.html',
  styleUrls: ['./client-detail.component.css']
})
export class ClientDetailComponent implements OnInit {
  @Input() client: Client;
  editMode = false;
  investor: Investor;
  investors: Investor[];
  availableInvestors: Investor[];

  constructor(
    private clientService: ClientService,
    private investorService: InvestorService,
    private route: ActivatedRoute,
    private location: Location
  ) { }

  ngOnInit() {
    this.getClient();
  }

  getClient(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.clientService.getClient(id)
      .subscribe(client => {
        this.client = client;
        this.getInvestorsForClient();
      });
  }

  getInvestorsForClient(): void {
    this.clientService.getClientInvestors(this.client)
      .subscribe(investors => this.investors = investors);
  }

  addInvestorMenu(): void {
    this.investorService.getInvestors()
      .subscribe(investors => this.availableInvestors = investors);
  }

  addInvestorForClient(id: string): void {
    console.log('Investor to add: ' + id + ' type: ' + (id + id));
    this.investor = this.availableInvestors.filter(i => i.id.toString() === id)[0];
    this.investor.client = this.client;
    this.investorService.updateInvestor(this.investor)
      .subscribe();
  }

  save(): void {
    this.clientService.updateClient(this.client)
      .subscribe();
  }

  goBack(): void {
    this.location.back();
  }

}
