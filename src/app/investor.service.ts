import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import {MessageService} from './message.service';
import {Investor} from './investor';
import {Fund} from './fund';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class InvestorService {

  private investorApi = 'http://localhost:8080/dashboard/investor';
  private fundApi = 'http://localhost:8080/dashboard/fund';

  constructor(
    private messageService: MessageService,
    private http: HttpClient
  ) { }

  /** Log a InvestorService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`InvestorService: ${message}`);
  }

  getInvestor(id: number): Observable<Investor> {
    this.messageService.add('Fetching investor with ID: ' + id);
    return this.http.get<Investor>(this.investorApi + '/' + id);
  }

  /**
   * Get all investors
   */
  getInvestors(): Observable<Investor[]> {
    return this.http.get<Investor[]>(this.investorApi)
      .pipe(catchError(this.handleError<Investor[]>('getInvestors', [])));
  }

  /**
   * Update investor through PUT request
   * @param investor - a Investor that will be updated
   */
  updateInvestor(investor: Investor): Observable<any> {
    return this.http.put(this.investorApi + '/' + investor.id, investor, httpOptions).pipe(
      tap(_ => this.log(`updated investor with id=${investor.id}`)),
      catchError(this.handleError<any>('updateInvestor'))
    );
  }

  /**
   * Add new investor method
   * @param investor - a Investor that will be added to application
   */
  addInvestor(investor: Investor): Observable<Investor> {
    return this.http.post(this.investorApi, investor, httpOptions).pipe(
      tap(_ => this.log(`added investor with name=${investor.name}`)),
      catchError(this.handleError<any>('addInvestor'))
    );
  }

  /**
   * Delete existed investor.
   * @param investor - a Investor that need to be deleted
   */
  deleteInvestor(investor: Investor): Observable<Investor> {
    return this.http.delete(`${this.investorApi}/${investor.id}`, httpOptions).pipe(
      tap(_ => this.log(`deleted investor with name=${investor.name}`)),
      catchError(this.handleError<any>('deleteInvestor'))
    );
  }

  /**
   * Get list of funds for the investor
   */
  getInvestorFunds(investor: Investor): Observable<Fund[]> {
    const uri = `${this.investorApi}/${investor.id}/fund`;
    return this.http.get<Fund[]>(uri).pipe(
      catchError(this.handleError<any>('getInvestorFunds'))
    );
  }

  /**
   * Get list of all funds
   */
  getFunds(): Observable<Fund[]> {
    return this.http.get<Fund[]>(this.fundApi).pipe(
      catchError(this.handleError<any>('getFunds'))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); // log to console
      this.log(`${operation} failed: ${error.message}`);
      // Keep app running by returning an empty result.
      return of(result as T);
    };
  }
}
