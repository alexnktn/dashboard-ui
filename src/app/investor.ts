import {Client} from './client';

export class Investor {
  id: number;
  name: string;
  description: string;
  client: Client;
}
