import { Component, OnInit } from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

import {InvestorService} from '../investor.service';
import {ModalContentComponent} from '../modal-content/modal-content.component';
import {Investor} from '../investor';
import {MessageService} from '../message.service';

@Component({
  selector: 'app-investors',
  templateUrl: './investors.component.html',
  styleUrls: ['./investors.component.css']
})
export class InvestorsComponent implements OnInit {
  investor: Investor;
  investors: Investor[];

  constructor(
    private investorService: InvestorService,
    private modalService: NgbModal,
    private messageService: MessageService
    ) { }

  ngOnInit() {
    this.getInvestors();
  }

  getInvestors(): void {
    this.investorService.getInvestors()
      .subscribe(investors => this.investors = investors);
  }

  showCrudMenu(investor: Investor): void {
    if (!investor) {
      investor = {id: null, name: null, description: null, client: null};
    }
    const modalRef = this.modalService.open(ModalContentComponent);
    modalRef.componentInstance.entity = investor;
    modalRef.componentInstance.passEntry.subscribe((receivedInvestor) => {
      this.investor = receivedInvestor;
      if (!receivedInvestor.id) {
        this.addInvestor();
      } else {
        this.updateInvestor();
      }
    });
  }

  addInvestor(): void {
    this.investorService.addInvestor(this.investor)
      .subscribe(() => {
        this.messageService.showSuccessMessage('Successfully added new investor!');
        this.getInvestors();
      });
  }

  updateInvestor(): void {
    this.investorService.updateInvestor(this.investor)
      .subscribe();
  }

  /**
   * Delete investor and update the investor array
   */
  deleteClient(investor: Investor): void {
    this.investorService.deleteInvestor(investor)
      .subscribe();
    this.investors = this.investors.filter(i => i !== investor);
  }

}
