import {Component, Input, OnInit} from '@angular/core';
import { Location } from '@angular/common';

import {Investor} from '../investor';
import {InvestorService} from '../investor.service';
import {ActivatedRoute} from '@angular/router';
import {Fund} from '../fund';

@Component({
  selector: 'app-investor-detail',
  templateUrl: './investor-detail.component.html',
  styleUrls: ['./investor-detail.component.css']
})
export class InvestorDetailComponent implements OnInit {
  @Input() investor: Investor;
  editMode = false;
  funds: Fund[];
  availableFunds: Fund[];

  constructor(
    private investorService: InvestorService,
    private route: ActivatedRoute,
    private location: Location
  ) { }

  ngOnInit() {
    this.getInvestor();
  }

  getInvestor(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.investorService.getInvestor(id)
      .subscribe(investor => {
        this.investor = investor;
        this.getFundsForInvestor();
      });
  }

  getFundsForInvestor(): void {
    this.investorService.getInvestorFunds(this.investor)
      .subscribe(funds => this.funds = funds);
  }

  save(): void {
    this.investorService.updateInvestor(this.investor)
      .subscribe();
  }

  addFundMenu(): void {
    this.investorService.getFunds()
      .subscribe(funds => this.availableFunds = funds);
  }

  addFundForInvestor(id: string): void {
    console.log('Investor to add: ' + id + ' type: ' + (id + id));
    const fund = this.availableFunds.filter(i => i.id.toString() === id)[0];
    fund.investor = this.investor;
    this.investorService.updateInvestor(this.investor)
      .subscribe();
  }
}
