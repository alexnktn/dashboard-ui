import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClientsComponent } from './clients/clients.component';
import { InvestorsComponent } from './investors/investors.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ClientDetailComponent } from './client-detail/client-detail.component';
import {InvestorDetailComponent} from './investor-detail/investor-detail.component';
import {FundsComponent} from './funds/funds.component';

const routes: Routes = [
  {path: '', redirectTo: '/dashboard', pathMatch: 'full'},
  {path: 'dashboard', component: DashboardComponent},
  {path: 'clients', component: ClientsComponent},
  {path: 'client/:id', component: ClientDetailComponent},
  {path: 'investors', component: InvestorsComponent},
  {path: 'investor/:id', component: InvestorDetailComponent},
  {path: 'funds', component: FundsComponent}
];
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
