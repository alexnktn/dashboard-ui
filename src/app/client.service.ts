import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Client } from './client';
import { Investor } from './investor';
import { MessageService } from './message.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  private clientApi = 'http://localhost:8080/dashboard/client';

  constructor(
    private messageService: MessageService,
    private http: HttpClient
  ) { }

  /** Log a ClientService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`ClientService: ${message}`);
  }

  getClient(id: number): Observable<Client> {
    this.messageService.add('Fetching user with ID: ' + id);
    return this.http.get<Client>(this.clientApi + '/' + id);
  }

  getClients(): Observable<Client[]> {
    return this.http.get<Client[]>(this.clientApi)
      .pipe(catchError(this.handleError<Client[]>('getClients', [])));
  }

  /**
   * Update client through PUT request
   * @param client - a Client that will be updated
   */
  updateClient(client: Client): Observable<any> {
    return this.http.put(this.clientApi + '/' + client.id, client, httpOptions).pipe(
      tap(_ => this.log(`updated client id=${client.id}`)),
      catchError(this.handleError<any>('updateClient'))
    );
  }

  /**
   * Add new client method
   * @param client - a Client that will be added to application
   */
  addClient(client: Client): Observable<Client> {
    return this.http.post(this.clientApi, client, httpOptions).pipe(
      tap(_ => this.log(`added client with name=${client.name}`)),
      catchError(this.handleError<any>('addClient'))
    );
  }

  /**
   * Delete existed client.
   * @param client - a Client that need to be deleted
   */
  deleteClient(client: Client): Observable<Client> {
    return this.http.delete(`${this.clientApi}/${client.id}`, httpOptions).pipe(
      tap(_ => this.log(`deleted client with name=${client.name}`)),
      catchError(this.handleError<any>('deleteClient'))
    );
  }

  /**
   * Get list of investor for the client
   */
  getClientInvestors(client: Client): Observable<Investor[]> {
    const uri = `${this.clientApi}/${client.id}/investor`;
    return this.http.get<Investor[]>(uri).pipe(
      catchError(this.handleError<any>('getClientInvestors'))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
        console.error(error); // log to console
        this.log(`${operation} failed: ${error.message}`);
        // Keep app running by returning an empty result.
        return of(result as T);
    };
  }
}
