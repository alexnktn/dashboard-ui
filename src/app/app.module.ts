import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { MatDialogModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { ClientsComponent } from './clients/clients.component';
import { ClientDetailComponent } from './client-detail/client-detail.component';
import { MessagesComponent } from './messages/messages.component';
import { InvestorsComponent } from './investors/investors.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ModalContentComponent } from './modal-content/modal-content.component';
import { InvestorDetailComponent } from './investor-detail/investor-detail.component';
import { FundsComponent } from './funds/funds.component';

@NgModule({
  declarations: [
    AppComponent,
    ClientsComponent,
    ClientDetailComponent,
    MessagesComponent,
    InvestorsComponent,
    DashboardComponent,
    ModalContentComponent,
    InvestorDetailComponent,
    FundsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    MatDialogModule,
    BrowserAnimationsModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [ModalContentComponent]
})
export class AppModule { }
