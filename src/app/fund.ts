import {Investor} from './investor';

export class Fund {
  id: number;
  name: string;
  description: string;
  totalFundAmount: number;
  investor: Investor;
}
